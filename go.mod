module gitlab.com/rouxware/ximenu

go 1.18

require bitbucket.org/EScottDaniels/sketch v1.0.1


require (
	bitbucket.org/EScottDaniels/colour v0.0.0-20220119032347-25336bea750c // indirect
	github.com/att/gopkgs v0.0.0-20200217010645-92f3312ebad2 // indirect
)
