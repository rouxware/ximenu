/*
	Abstract:	Constants for the package

	Date:		18 April 2023
	Author:		E. Scott Daniels
*/

package ximenu


type Action int
const (
	Nothing	Action = iota	// nothing happens
	Show					// show a button set
	Hide					// hide a button set
	Command					// execute a command
)

/*
	Currently the sketch package doesn't provide constants, so these are brittle
	given that they must match constants in the underlying XI package.
*/
type XIButtonKind int
const (
	UNKNOWN_BUTTON	XIButtonKind = 0
	RADIO_BUTTON	XIButtonKind  = 1
	STICKY_BUTTON	XIButtonKind  = 2	// a.k.a XI toggle button
	SPRING_BUTTON	XIButtonKind  = 3
	ENTRY_BUTTON	XIButtonKind  = 4	// who knows what this is

	PRESSED			int = 0
	RELEASED		int = 1
)

type XImState int
const (
	XImDOWN	XImState = iota	// button is down (in)
	XImUP 						// button is up (out)
)
