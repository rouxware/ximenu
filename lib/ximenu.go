
/*
	Abstract:	A menu system on top of sketch.

	Date:		10 April 2023
	Author:		E. Scott Daniels
*/

package ximenu

import (
	"fmt"
	"os"
	"sync"
	"time"

	sk "bitbucket.org/EScottDaniels/sketch"
	sktools "bitbucket.org/EScottDaniels/sketch/tools"
)

const (
	HIDDEN bool = false	// parameters expected by set_vis gc method
	VISIBLE bool = true
)


type XImenu struct {
	sync.Mutex
	gc		sk.Graphics_api			// the sketch interface to the window
	bSets	map[string]*ButtonSet	// button sets that are in the menu
	sid2Set	map[int]*ButtonSet		// ability to map a set id from a button id to the set info
	bSetId	int						// next button set id
	lChan	chan *sktools.Interaction // channel from the XI world
}

/*
	Create a new window with sketch. The "output" for a sketch window
	defines the display id (we leave blank to default to DISPLAY),
	height, width and window title.
*/

func NewXiMenu( title string, width int, height int, ) *XImenu {
	outputDesc := fmt.Sprintf(",%d,%d,%s", height, width, title ) // uses value off DISPLAY; first parameter is empty
	//fmt.Fprintf( os.Stderr, "new menu>>> height=%d width=%d od:%s\n", height, width, outputDesc )
	gc, err := sk.Mk_graphics_api( "xi", outputDesc )		// kind must always be xi
	if err != nil {
		fmt.Fprintf( os.Stderr, "### ERR ### unable to create XI sketch output: %s", err )
		return nil
	}

	menu := &XImenu {
		gc: 		gc,
		bSets:		make( map[string]*ButtonSet ),
		sid2Set:	make( map[int]*ButtonSet ),
		bSetId:		1,
		lChan:		make( chan *sktools.Interaction, 1024 ),
	}

	return menu
}

/*
	Events for all button sets arrive on a single channel that this
	method listens to. The events from XI are MOUSE button events, not
	soft button events. We should onl react to a release.
*/
func (xim *XImenu) listener( ) {
	if xim == nil {
		return
	}

	//fmt.Fprintf( os.Stderr, "button event listener started\n")
	for {
		e := <- xim.lChan
		movement := e.Kind		// event kind, not button kind
		if movement != RELEASED {
			continue
		}

		bid := e.Data
		sid := bid >> 8		// the set ID owning the button

		xim.Lock()
		bs, there := xim.sid2Set[sid]
		xim.Unlock()
		if ! there {
			fmt.Fprintf( os.Stderr, "### ERR ### internal mishap: unrecognised set id: bid=0x%x sid=%d %#v\n", sid, bid, e )
			continue
		}

		ev := &XImEvent {}
		b := bs.Button( bid )
		ev.Button = b
		//fmt.Fprintf( os.Stderr, "button event id=%d movement=%d kind=%d bisnil=%v\n", bid, movement, int(ev.Button.Kind()), ev.Button == nil )

		switch ev.Button.Kind() {
			case RADIO_BUTTON:
				if b.IsUp() {
					lb := bs.ToggleLast()
					if lb != nil {
						lev := &XImEvent {		// we pop the last one first, seond event for that
							Button:	lb,
							State:	lb.State(),
						}
						bs.uChan <- lev			// pushed ahead of the button just clicked on
					}
					
					b.ToggleState()			// now toggle the clicked on button
					bs.SetLast( b )			// and capture it as the last one affected in the radio group
				} else {
					continue				// already down, there is no action
				}

			case STICKY_BUTTON:
				b.ToggleState()

			case SPRING_BUTTON:
				// no state change; spring buttons are always up

			default:
				fmt.Fprintf( os.Stderr, "### ERR ### internal mishap: unrecognised button kind: %d %#v\n", int(ev.Button.Kind()), e )
		}

		ev.State = b.State()
		bs.uChan <- ev
	}
}

/*
	Add a button set to the menu.
	Returns the channel that the user can listen on for button
	events.
*/
func (xim *XImenu) AddButtonSet( name string, x int, y int, width int, height int )  chan *XImEvent {
	if xim == nil {
		return nil
	}

	xim.Lock()
	if ! xim.gc.Select_subpage("root") {
		fmt.Fprintf(os.Stderr, "###WARN### select root subpage failed\n" )
	}
	xim.gc.Mk_subpage( name, float64(x), float64(y), float64(height), float64(width) )
	setID := xim.bSetId
	setBase := xim.bSetId << 8	// button base allows button ID to be added
	xim.bSetId++
	xim.Unlock()

	bs := NewButtonSet( name, setBase, x, y, width, height )
	xim.Lock()	
	xim.bSets[name] = bs
	xim.sid2Set[setID] = bs
	xim.Unlock()

	return bs.MkChan()
}



/*
	Add a button to the named set. For now button size is used only on
	the first invocation of the method; at somepoint the sketch package
	might allow for different sized buttons in the same menu. Returns the
	button ID assigned by the underlying manager or -1 on error.

	The XI interface supports only one button listener, so the button ID
	must be <set-id><button-id>. We limit the button ID to 256; that seems
	plenty for a single set (just like 640k was plenty, right Bill?).
*/
func (xim *XImenu) AddButton( setName string, label string, kind XIButtonKind, width int, height int, colour string, bgColour string ) *Button {
	if xim == nil || xim.gc == nil {
		return nil
	}

	xim.Lock()
	bs, there := xim.bSets[setName]
	xim.Unlock()
	if ! there {
		fmt.Fprintf( os.Stderr, "### ERR ### AddButton: internal mishap: setname unknown: %s", setName)
		return nil
	}

	xim.gc.Select_subpage( "root" )  // all pages are hung off of the root
	if ! xim.gc.Select_subpage( setName ) {	// make this the active page
		fmt.Fprintf(os.Stderr, "###WARN### select subpage for %s failed\n", setName )
	}

	if bs.nextX + width > bs.dispWidth {
		if bs.nextX >= 0 {
			bs.nextX = 0
			bs.nextY += bs.lastHeight + 3
		}
	}
	x := bs.nextX + 3
	y := bs.nextY
	bs.nextX += width + 3
	bs.lastHeight = height
	bid := bs.id + bs.nextBid		// only one XI listner so the button id is <set-id><bid>
	bs.nextBid++
	button := NewButton( bid, kind, x, y, width, height, colour, bgColour )
	button.SetLabel( label )		// buttons are NOT inserted until subpage is shown
	bs.bid2btn[bid] = button

	xim.gc.Select_subpage( "root" )
	return button
}

/*
func (xim *XImenu) Clear( setName string ) {
	if xim == nil || xim.gc == nil {
		return
	}

	xim.Lock()
	_, there := xim.bSets[setName]
	xim.Unlock()
	if there {
		xim.gc.Select_subpage( "root" )
		xim.gc.Clear_subpage( "#000000" )
	}
}
*/

func (xim *XImenu) PaintButtonSet( setName string ) {
	if xim == nil || xim.gc == nil {
		return
	}

	xim.Lock()
	bs, there := xim.bSets[setName]
	xim.Unlock()
	if ! there {
		return
	}

	if ! xim.gc.Select_subpage( setName ) {
		return
	}

	bs.Paint( xim.gc )
}

/*
	Button sets are painted inside of subpages which are created when the subpage
	is shown and are deleted when the subpage is hidden. When show is called,
	the subpage is created and named, and the button set is painted inside of it.
*/
func (xim *XImenu) Show( setName string ) {
	if xim == nil || xim.gc == nil {
		return
	}

	xim.Lock()
	bs, there := xim.bSets[setName]
	xim.Unlock()
	if there {
		x, y, width, height := bs.DispInfo()
		xim.gc.Select_subpage( "root" )
		xim.gc.Mk_subpage( setName, float64(x), float64(y), float64(height), float64(width) )
		xim.gc.Select_subpage( setName )
		xim.gc.Add_listener( xim.lChan )
		bs.Paint( xim.gc )
	}
}

/*
	Button sets are hidden by removing the subpage. Sounds brutal, but
	it's the way the XI stuff works.
*/
func (xim *XImenu) Hide( setName string ) {
	if xim == nil || xim.gc == nil {
		return
	}

	xim.Lock()
	_, there := xim.bSets[setName]
	xim.Unlock()
	if there {
		xim.gc.Select_subpage( "root" )
		xim.gc.Delete_subpage( setName )
	}
}

/*
	Start our listener and then drive things.
*/
func (xim *XImenu) Engage() {
	if xim == nil || xim.gc == nil {
		return
	}

	xim.Lock()
	for bsName := range xim.bSets {
		xim.gc.Select_subpage( bsName )
		xim.gc.Add_listener( xim.lChan )
	}
	xim.Unlock()

	go xim.listener()
	for {
		xim.gc.Show()
		xim.gc.Drive()
		time.Sleep( 10 * time.Millisecond )
	}
}

// ------------------------------------------------------------------------------
/*
	Pushed onto a listener's channel when a button state changes.
*/
type  XImEvent struct {
	Button *Button
	State XImState
}

