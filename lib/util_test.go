
/*
	Abstract:	Test support functions
*/

package ximenu

import(
	"fmt"
	"os"
	"testing"
)

func failOnErr( t *testing.T, err error, msg string ) {
	if err != nil {
		fmt.Fprintf( os.Stderr, "### ERR ###  %s: %s\n", msg, err )
		t.Fail()
		os.Exit( 1 )
	}
}

/*
	Returns 1 on error so errors can be counted if needed.
*/
func failIfFalse( t *testing.T, state bool, msg string ) int {
	if !state {
		fmt.Fprintf( os.Stderr, "### FAIL ###  %s\n", msg )
		t.Fail()
		return 1
	}

	return 0
}

/*
	If condition is false then we abort after writing the message.
*/
func abortIfFalse( t *testing.T, state bool, msg string ) {
	if !state {
		fmt.Fprintf( os.Stderr, "### ABEND ###  %s\n", msg )
		t.Fail()
		os.Exit(1)
	}
}
