/*
	Abstract:	Tracks a button's activity and identity

	Date:		17 April 2023
	Author:		E. Scott Daniels
*/

package ximenu

import( 
	sk "bitbucket.org/EScottDaniels/sketch"
)

type Button struct {
	id		int
	kind	XIButtonKind
	state	XImState		// up or down
	pAction	Action			// what we will do when the button is pressed
	rAction Action			// what we do when released
	pTarget	string			// the target of the push action (set name, command string, etc)
	rTarget	string			// the target of the release action
	label	string			// what is painted on the button
	x		int				// placement and geometry info
	y		int
	width	int
	height	int
	bgColour	string
	fgColour	string
}

//func NewButton( id int, kind XIButtonKind, pAction Action, rAction Action, pTarget string, rTarget string ) *Button {
func NewButton( id int, kind XIButtonKind, x int, y int, width int, height int, fgColour string, bgColour string ) *Button {
	return &Button {
		id:			id,
		kind:		kind,
		x:			x,
		y:			y,
		width:		width,
		height:		height,
		fgColour:	fgColour,
		bgColour:	bgColour,
		state:		XImUP,
	}
}

/*
	Creates a new button, but only with the id and kind.
*/
func EmptyButton( id int, kind XIButtonKind ) *Button {
	return &Button {
		id:			id,
		kind:		kind,
		state:		XImUP,
		pAction:	Nothing,
		rAction:	Nothing,
	}
}

/*
	Add the action/type for a button press.
*/
func (b *Button) AddPressAT( action Action, target string) {
	if b == nil {
		return
	}

	b.pAction = action
	b.pTarget = target
}

/*
	Add the action/type for a button release.
*/
func (b *Button) AddRelAT( action Action, target string) {
	if b == nil {
		return
	}

	b.rAction = action
	b.rTarget = target
}

/*
	Return the action and target corresponding to the current
	state of the button (pressed or released).
*/
func(b *Button) ActionTarget() (Action, string) {
	if b == nil {
		return Nothing, ""
	}
	if b.state == XImUP {
		return b.rAction, b.rTarget
	}

	return b.pAction, b.pTarget
}

/*
	Get the press action and target.
*/
func(b *Button) GetPressAT() (Action, string) {
	if b == nil {
		return Nothing, ""
	}

	return b.pAction, b.pTarget
}

/*
	Get the release action and target.
*/
func(b *Button) GetRelAT() (Action, string) {
	if b == nil {
		return Nothing, ""
	}

	return b.rAction, b.rTarget
}

/*
	Returns true if he button is down (pressed)
*/
func (b *Button) IsDown( ) bool {
	if b == nil {
		return false
	}
	return b.state == XImDOWN
}

/*
	Returns True if the button is up (not pressed)
*/
func (b *Button) IsUp( ) bool {
	if b == nil {
		return false
	}
	return b.state == XImUP
}


/*
	Return the kind of button this is
*/
func (b *Button) Kind() XIButtonKind {
	if b == nil {
		return UNKNOWN_BUTTON
	}

	return b.kind
}

/*
	Given a graphcs context (an interface) insert the button into the
	active subpage.
*/
func (b *Button) Insert( gc sk.Graphics_api ) {
	if b == nil {
		return
	}

	gc.Add_button( b.id, b.x, b.y, b.height, b.width, b.label, b.bgColour, b.fgColour, int( b.kind ), b.state == XImDOWN )
}

/*
	Set the button label (changed next paint operation)
*/
func (b *Button) SetLabel( newLabel string ) {
	if b == nil {
		return
	}

	b.label = newLabel
}


/*
	Returns True if the button is up (not pressed)
*/
func (b *Button) State( ) XImState {
	if b == nil {
		return XImUP
	}
	return b.state
}

/*
	Flip the button state.
*/
func (b *Button) ToggleState( ) {
	if b == nil {
		return
	}

	if b.state == XImDOWN {
		b.state = XImUP
	} else {
		b.state = XImDOWN
	}
}
