/*
/*
	Abstract:	Description parser reads a description file (.xim) and generates
				a description struct that can be used to populate the menu in XI.

				Json sucks because of the need to quote keys and lack of comments. 
				YAML sucks because of the need to be indention sensitive. Besides,
				writing a parser isn't hard, so we get an easy to manage menu
				description file, and no bullshit.
						
					menu:
						path:  string
						title: string
						geom:	w,h
						colours:	fg,bg
						import:	filename

						buttonset:
							name:
							pos:	x,y
							geom:	w,h
							bsize:	w,h
							visible:	true/yes|false/no
							colours

							button:
								kind:
								geom:	w,h
								label:
								press:
								release:
								target:
								colours:


	Date:		22 April 2023
	Author:		E. Scott Daniels
*/

package ximenu

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

/*
	Given a value in the buffer defrock it such that:
		- Lead/trail white space is removed
		- Trailing comment of the form <whitespace>#<text> is removed
		- Quotes are removed. If a string has blanks is must be quoted.
*/
func defrockValue( buf string ) string {
	b := strings.Trim( buf, " \t\n" )		// trim, so we can cut <whitespae>#<comment>

	if len( b ) <= 0 {
		return ""
	}

	if b[0] == '"' {					// strip quotes, and ditch all after last quote
		b = b[1:]
		i := strings.Index( b, "\"" )
		if i == 0 {
			return ""
		} else {
			if i > 0 {
				b = b[:i]
			}
		} 
	} else {								// allow '6  # a magic number', stripping the comments
		i := strings.IndexAny( b, " \t#" )
		if i > 0 {
			b = b[:i]						// ditch trailing goop
		} else {
			if  i == 0 {
				b = ""
			}
		}
	}

	return b
}

/*
	C-Like conversion of buf to integer.
*/
func atoi( buf string ) int {
	b := defrockValue( buf )
	v, err := strconv.ParseInt( b, 10, 64 )
	if err != nil {
		return 0
	}

	return int( v )
}

/*
	parse the string for x,y where x and y are integers. Converts
	the string representations to integers in a C-like manner.	
*/
func getIntPair( buf string ) (x int, y int) {
	tokens := strings.SplitN( buf, ",", 2 )
	
	x = atoi( tokens[0] )
	if len( tokens ) > 1 {
		y = atoi( tokens[1] )
	} else {
		y = 0
	}

	return x, y
}

/*
	Returns the path based on pstr. If pstr is $* then * is assumed
	to be an environment variable name and is looked up. If not,
	then pstr is returned.
*/
func getPath( pStr string ) string {
	if pStr[0:1] == "$" {
		return os.Getenv( pStr[1:] )
	} else {
		return pStr
	}
}

/*
	parse the string for x,y where x and y are strings. Unquoted
	white space and trailing comments are removed.

	BUG: for now a comma cannot be nested in the quoted string.
*/
func getStrPair( buf string ) (x string, y string) {
	tokens := strings.SplitN( buf, ",", 2 )
	
	x = defrockValue( tokens[0] )
	if len( tokens ) > 1 {
		y = defrockValue( tokens[1] )
	} else {
		y = ""
	}

	return x, y
}

/*
	Parse the button section and fill in a button description.
	The descriptor and the unrecognised record that prompted
	the return are returned.

	We allow imbed at the button level, but beware: the imbed
	will 'pop' on the first unrecognised token in the input
	stream (in addition to eof).
*/
func ParseButtonSect( brs *BReaderStack ) (*DescButton, string) {

	btn := &DescButton {
		Geom: DescGeom {
			Width: 0,		// default to using buttonSet sizes
			Height: 0,
		},
	}

	for {
		rec, err := brs.Next( )
		if err != nil {
			return btn, ""
		}

		tokens := strings.SplitN( rec, ":", 2 )
		key := defrockValue( tokens[0] )
		switch key {
			case "", "\n":		// skip blanks

			case "colours", "colour":
				f, b := getStrPair( tokens[1] )
				btn.FgColour = f
				btn.BgColour = b

			case "geom":
				w, h := getIntPair( tokens[1] )
				btn.Geom.Width = w
				btn.Geom.Height = h

			case "include", "import":		// each section must handle the import
				err = brs.Open( defrockValue( tokens[1] ) )
				if err != nil {
					fmt.Fprintf( os.Stderr, "### ERR ### cant import %s: %s\n", defrockValue(tokens[1]), err )
					return nil, ""
				}

			case "kind":
				btn.Kind = defrockValue( tokens[1] )

			case "label":
				btn.Label = defrockValue( tokens[1] )

			case "press":
				btn.PressAction = defrockValue( tokens[1] )

			case "release", "rel":
				btn.RelAction = defrockValue( tokens[1] )

			case "target":
				btn.Target = defrockValue( tokens[1] )

			default:
				//fmt.Fprintf( os.Stderr, "### WARN ### ignoring unrecognised button section key: %q\n", key )
				return btn, rec
		}
	}

	return btn, ""
}

func ParseButtonSetSect( brs *BReaderStack ) (*DescBSet, string) {
	bs := &DescBSet {
		Buttons:	make( []*DescButton, 0 ),
		Pos: DescPosition {
			X:	0,
			Y:	0,
		},
		Geom: DescGeom {
			Width: 145,
			Height: 230,
		},
		BSize: DescGeom {
			Width: 70,
			Height: 70,
		},
	}
	
	var err error
	var btn *DescButton		// bloody go needs this to do the right thing with rec in button case
	rec := ""
	for {
		if rec == "" {
			rec, err = brs.Next()
			if err != nil {
				return bs, ""
			}
		}

		tokens := strings.SplitN( rec, ":", 2 )
		lastRec := rec
		rec = ""
		key := defrockValue( tokens[0] )
		switch key {
			case "", "\n":				// skip blanks

			case "button":
				btn, rec = ParseButtonSect( brs )
				bs.Buttons = append( bs.Buttons, btn )

			case "name":
				bs.Name = defrockValue( tokens[1] )

			case "visible":
				v := defrockValue( tokens[1] )
				switch v[0:1] {
					case "T", "t", "y", "Y":	// true True Yes yes
						bs.Visible = true

					case "F", "f", "n", "N":	// False, false, no, No
						bs.Visible = false

					default:
						fmt.Fprintf( os.Stderr, "bad boolean value for visible: %q\n", tokens[1] )
						bs.Visible = false
				}

			case "bsize":
				w, h := getIntPair( tokens[1] )
				bs.BSize.Width = w
				bs.BSize.Height = h

			case "colours", "colour":
				f, b := getStrPair( tokens[1] )
				bs.FgColour = f
				bs.BgColour = b

			case "geom", "geometry":
				w, h := getIntPair( tokens[1] )
				bs.Geom.Width = w
				bs.Geom.Height = h

			case "include", "import":		// each section must handle the import
				err = brs.Open( defrockValue( tokens[1] ) )
				if err != nil {
					fmt.Fprintf( os.Stderr, "### ERR ### cant import %q: %s\n", defrockValue(tokens[1]), err )
					return nil, ""
				}

			case "pos", "position":
				x, y := getIntPair( tokens[1] )
				bs.Pos.X = x
				bs.Pos.Y = y
	
			default:
				return bs, lastRec
				//fmt.Fprintf( os.Stderr, "### WARN ### ignoring unrecognised buttonSet section key: %q\n", key )
		}
	}

	return bs, ""
}

/*
	The description loaded in the more straight forward, internal,
	format.
*/
func DLoadXIM( fName string ) (*Description, error) {
	brs := NewBReaderStack( 25 )
	err := brs.Open( fName )
	if err != nil {
		return nil, err
	}

	desc := &Description{
		Sets:	make( []*DescBSet, 0 ),
	}

	var bs *DescBSet	// bloody go needs this for buttonset case
	rec := ""
	for {
		if rec == "" {
			rec, err = brs.Next( )
		}
		if err != nil {    // most likely end of file
			break
		}

		tokens := strings.SplitN( rec, ":", 2 )
		rec = ""
		key := defrockValue( tokens[0] )
		switch key {
			case "", "\n":	// ignore blank records

			case "menu":	// not needed, but it looks nice in the file
				// fmt.Fprintf( os.Stderr, "building description starts\n" )

			case "buttonset", "buttonSet":
				bs, rec = ParseButtonSetSect( brs )
				desc.Sets = append( desc.Sets, bs )

			case "colours":
				f, b := getStrPair( tokens[1] )
				desc.FgColour = f
				desc.BgColour = b

			case "geom", "geometry":
				w, h := getIntPair( tokens[1] )
				desc.Geom.Width = w
				desc.Geom.Height = h

			case "include", "import":
				err := brs.Open( defrockValue( tokens[1] ) )
				if err != nil {
					fmt.Fprintf( os.Stderr, "### ERR ### unable to open file: %q\n", defrockValue( tokens[1] ) )
				}

			case "path":	// the path use when opening include files; if $name then we assume name is an env var
				brs.SetPath( getPath( defrockValue( tokens[1] ) ) )	// path handles "", so this is safe

			case "title":
				desc.Title = defrockValue( tokens[1] )

			default:
				fmt.Fprintf( os.Stderr, "### WARN ### ignoring unrecognised key: %s\n", key )
		}
	}

	if err != nil {
		if fmt.Sprintf("%s", err) == "EOF" {
			err = nil
		}
	}
	return desc, err
}
