
/*
	Abstract:	A generalised button reader with simplistic command execution.
				The main .xim file is expected to be given as the only parameter
				on the command line. This is read to generate a list of button
				sets which are added to the menu. Once added, the menu is displayed
				and the callback method is used to act on button presses.

	Date:		30 April 2023
	Author:		E. Scott Daniels
*/

package main

import (
	"fmt"
	"flag"
	"os"
	"os/exec"
	"strings"

	xim "gitlab.com/rouxware/ximenu/lib"
)

/*
	A simple command driver that makes no effort to worry about quoted strings.
	This will split the string on semicolons and execute each substring as a
	separate command. This should allow "cease -r; start ...."
*/
func drive( cmdStr string ) {
	cmds := strings.Split( cmdStr, ";"  )

	sdir := os.Getenv( "SADAR_ROOT" )
	if sdir == "" {
		sdir = os.Getenv( "HOME" )
		sdir += "/.sadar"
	}
	sdir += "/lib"

	for _, c := range cmds {
		//fmt.Fprintf( os.Stderr, "running %q\n", c )
		c = strings.Trim( c, " \t\n" )
		tokens := strings.Split( c, " " )
		cmd := exec.Command( tokens[0], tokens[1:]... )
		cmd.Dir = sdir
		err := cmd.Run()
		if err != nil {
			fmt.Fprintf( os.Stderr, "### ERR ### execution failed: %s: %s\n", c, err)
			return
		}
	}
}

/*
	Our listener for events coming from XImenu. We expect action to be
	one of: show, cmd or hide.  For show/hide, the target can be one
	or more menu names (comma separated). For cmd, the target is the
	command to run.
*/
func ears( menu *xim.XImenu, ch chan *xim.XImEvent ) {
	//eData := ""		// data entered via absorb

	for {
		e := <-ch

		if e != nil {
			//fmt.Fprintf( os.Stderr, "got menu event: %#v\n", e )
			action, target := e.Button.ActionTarget()
			switch action {
				/*
				case xim.Activate:	// activate an entry "field"

				case xim.Append:	// append to the active field
					eData += target

				case xim.Prepend:   // prepend to the active field
					eData = target + eData

				case xim.SetField:
				*/

				case xim.Show:
					//fmt.Fprintf( os.Stderr, "\t show: %q\n", target )
					tokens := strings.Split( target, "," )
					for _, tok := range tokens {
						tok = strings.Trim( tok, " \t\n" )
						menu.Show( tok )
					}

				case xim.Hide:
					//fmt.Fprintf( os.Stderr, "\t hide: %q\n", target )
					tokens := strings.Split( target, "," )
					for _, tok := range tokens {
						tok = strings.Trim( tok, " \t\n" )
						menu.Hide( tok )
					}

				case xim.Command:
					fmt.Fprintf( os.Stderr, "\t run command: %q\n", target )
					drive( target )

				default:
					fmt.Fprintf( os.Stderr, "\t no action for this button event/state\n" )
			}
		}
	}
}

func main() {
	flag.Parse()
	args := flag.Args()

	fName := "demo.xim"
	if len( args ) > 0 {
		fName = args[0]
	}

	md, err := xim.DLoadXIM( fName  )	// load the menu descriptors
	if err != nil {
		fmt.Fprintf( os.Stderr, "### ABEND ### could not initialise: %s\n", err )
		os.Exit( 1 )
	}

	title := md.MenuTitle()
	w, h := md.Geometry()
	menu := xim.NewXiMenu( title, w, h )

	i := 0
	for {						// create button sets
		dbs := md.BSetN( i )	// get the next descriptor button set
		if dbs == nil {
			break
		}	

		name := dbs.BsName()
		//fmt.Fprintf( os.Stderr, "creating button set: %s\n", name )
		x, y, w, h := dbs.Geometry()
		lchan := menu.AddButtonSet( name, x, y, w, h )

		j := 0
		for {
			dbtn := dbs.ButtonN( j )		// get the buttons and add them
			if dbtn == nil {
				break
			}

			kind, label := dbtn.KindLabel()
			//fmt.Fprintf( os.Stderr, "  adding button: %s\n", label )
			pAction, rAction, target := dbtn.ActionsTarget()	
			fg, bg := dbtn.Colours()
			w, h := dbtn.Geometry()
			b := menu.AddButton( name, label, kind, w, h, fg, bg )
			b.AddPressAT( pAction, target  )
			b.AddRelAT( rAction, target )
			j++
		}

		go ears( menu, lchan )	// finally start the listener for the set
		if dbs.IsVisible() {
			//fmt.Fprintf( os.Stderr, "showing button set: %s\n", name )
			menu.Show( name )
		}

		i++
	}

	menu.Engage()
}

